FROM mysql:${VERSION}
ADD ${SQL} /docker-entrypoint-initdb.d/
RUN chmod 777 /docker-entrypoint-initdb.d/*.*
